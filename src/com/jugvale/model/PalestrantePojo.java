package com.jugvale.model;

import java.io.Serializable;

public class PalestrantePojo implements Serializable {
	private static final long serialVersionUID = 3L;
	private String nome;
	private String miniCV;
	private String email;

	public PalestrantePojo() {
	}

	public PalestrantePojo(String nome, String cv) {
		this.nome = nome;
		this.miniCV = cv;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMiniCV() {
		return miniCV;
	}

	public void setMiniCV(String cv) {
		this.miniCV = cv;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
