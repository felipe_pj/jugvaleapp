package com.jugvale.model;

import java.io.Serializable;
import java.util.List;

public class EventoPojo implements Serializable {

	private static final long serialVersionUID = 4L;
	public static final String KEY = "Evento";
	private String edicao;
	private String dia;
	private String horaInicio;
	private String horaFim;
	private List<PalestraPojo> palestras;
	
	public EventoPojo() {
	}

	public EventoPojo(String edicao, String dia, String horaInicio,
			String horaFim, List<PalestraPojo> palestras) {
		this.edicao = edicao;
		this.dia = dia;
		this.horaInicio = horaInicio;
		this.horaFim = horaFim;
		this.palestras = palestras;
	}

	public String getEdicao() {
		return edicao;
	}

	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public List<PalestraPojo> getPalestras() {
		return palestras;
	}

	public void setPalestras(List<PalestraPojo> palestras) {
		this.palestras = palestras;
	}

}
