package com.jugvale.model;

import java.io.Serializable;
import java.util.List;

public class ListaPalestra implements Serializable {
	
	private static final long serialVersionUID = 2L;
	public  static final String KEY = "LISTA_PALESTRA";
	private List<PalestraPojo> palestras;
	
	public ListaPalestra(List<PalestraPojo> equipes) {
		this.palestras = equipes;
	}

	public List<PalestraPojo> getPalestras() {
		return palestras;
	}

	public void setPalestras(List<PalestraPojo> palestras) {
		this.palestras = palestras;
	}
	
}
