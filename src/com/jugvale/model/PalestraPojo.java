package com.jugvale.model;

import java.io.Serializable;

public class PalestraPojo implements Serializable {
	private static final long serialVersionUID = 1L;
	public final static String KEY = "PALESTRA";
	private String titulo;
	private String tema;
	private String horaApresentacao;
	private PalestrantePojo palestrante;

	public PalestraPojo() {
	}

	public PalestraPojo(String titulo, String tema, String horaApresentacao,
			PalestrantePojo palestrante) {
		this.titulo = titulo;
		this.tema = tema;
		this.horaApresentacao = horaApresentacao;
		this.palestrante = palestrante;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}

	public String getHoraApresentacao() {
		return horaApresentacao;
	}

	public void setHoraApresentacao(String horaApresentacao) {
		this.horaApresentacao = horaApresentacao;
	}

	public PalestrantePojo getPalestrante() {
		return palestrante;
	}

	public void setPalestrante(PalestrantePojo palestrante) {
		this.palestrante = palestrante;
	}

	public String getNomePalestrante() {
		return palestrante.getNome();
	}

}
