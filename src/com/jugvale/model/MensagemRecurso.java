package com.jugvale.model;

import java.io.Serializable;

public class MensagemRecurso implements Serializable {
	private static final long serialVersionUID = 6L;
	private String codigo;
	private String mensagem;

	public MensagemRecurso() {
	}

	public MensagemRecurso(String codigo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getMensagem() {
		return mensagem;
	}
}
