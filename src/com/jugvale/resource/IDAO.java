package com.jugvale.resource;

import java.util.List;

public interface IDAO<T> {
	
	public T load(int id) throws Exception;	
	public List<T> list() throws Exception;
	
}
