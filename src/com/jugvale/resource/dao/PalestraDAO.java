package com.jugvale.resource.dao;

import java.util.List;

import android.util.Log;

import com.jugvale.model.PalestraPojo;
import com.jugvale.resource.IDAO;
import com.jugvale.view.entitymap.MapPalestra;

public class PalestraDAO extends WSResource implements IDAO<PalestraPojo> {

	private MapPalestra mapPalestra = null;

	public PalestraDAO() {
		super();
	}

	@Override
	public PalestraPojo load(int id) throws Exception {
		return list().get(id);
	}

	@Override
	public List<PalestraPojo> list() throws Exception {
		String result;
		result = getRESTFileContent(URL.URL_EVENTO);

		if (result == null) {
			Log.e(LOG, "Falha ao acessar WS");
			return null;
		}
		jp = jsonFactory.createJsonParser(result);
		mapPalestra = objectMapper.readValue(jp, MapPalestra.class);

		return mapPalestra.get(PalestraPojo.KEY);

	}

}
