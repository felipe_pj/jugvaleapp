package com.jugvale.resource.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.jugvale.model.PalestraPojo;
import com.jugvale.model.PalestrantePojo;
import com.jugvale.resource.IDAO;

public class HipoteticaDAO implements IDAO<PalestraPojo>{

	List<PalestraPojo> lista = new ArrayList<PalestraPojo>();
	
	public HipoteticaDAO() {
		PalestraPojo pal1 = new PalestraPojo("Android", "Android", "08:00 hr", new PalestrantePojo("Felipe", "Univap"));
		PalestraPojo pal2 = new PalestraPojo("IOs", "IOs", "09:00 hr", new PalestrantePojo("Pedro", "Fatec"));
		PalestraPojo pal3 = new PalestraPojo("JBoss", "JBoss", "10:00 hr", new PalestrantePojo("Credenciado", "RedHat"));
		
		PalestraPojo pal4 = new PalestraPojo("Android", "Android", "08:00 hr", new PalestrantePojo("Felipe", "Univap"));
		PalestraPojo pal5 = new PalestraPojo("IOs", "IOs", "09:00 hr", new PalestrantePojo("Pedro", "Fatec"));
		PalestraPojo pal6 = new PalestraPojo("JBoss", "JBoss", "10:00 hr", new PalestrantePojo("Credenciado", "RedHat"));
		
		lista.addAll(Arrays.asList(pal1, pal2, pal3, pal4,pal5, pal6));
	}

	@Override
	public PalestraPojo load(int id) throws Exception {
		return lista.get(0);
	}

	@Override
	public List<PalestraPojo> list() throws Exception {
		return lista;
	}

}
