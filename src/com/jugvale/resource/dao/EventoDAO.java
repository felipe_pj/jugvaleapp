package com.jugvale.resource.dao;

import java.util.Arrays;
import java.util.List;

import android.util.Log;

import com.jugvale.model.EventoPojo;
import com.jugvale.resource.IDAO;

public class EventoDAO extends WSResource implements IDAO<EventoPojo> {

	private EventoPojo eventoPojo = null;

	public EventoDAO() {
		super();
	}

	@Override
	public EventoPojo load(int id) throws Exception {
		String result;
		result = getRESTFileContent(URL.URL_EVENTO);
		Log.i(LOG, "Result: " + result);
		if (result == null) {
			Log.e(LOG, "Falha ao acessar WS");
			return null;
		}
		jp = jsonFactory.createJsonParser(result);
		eventoPojo = objectMapper.readValue(jp, EventoPojo.class);

		return eventoPojo;
	}

	@Override
	public List<EventoPojo> list() throws Exception {
		return Arrays.asList(load(0));
	}

}
