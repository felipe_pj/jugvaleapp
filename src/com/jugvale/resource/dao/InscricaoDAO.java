package com.jugvale.resource.dao;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import android.util.Log;

import com.jugvale.model.MensagemRecurso;
import com.jugvale.model.UsuarioPojo;
import com.jugvale.resource.IDAO;

public class InscricaoDAO extends WSResource implements IDAO<UsuarioPojo> {

	public InscricaoDAO() {
		super();
	}

	@Override
	public UsuarioPojo load(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UsuarioPojo> list() throws Exception {
		return null;
	}
	
	public MensagemRecurso cadastrar(UsuarioPojo usuario) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(usuario);
		String result = postRESTFileContent(URL.URL_CADASTRAR_USUARIO, json);
		Log.e(LOG, "Result: " + result);
		if (result == null) {
			Log.e(LOG, "Falha ao acessar WS");
			return null;
		}
		jp = jsonFactory.createJsonParser(result);
		MensagemRecurso mensagem = objectMapper.readValue(jp, MensagemRecurso.class);
		return mensagem;
	}
	
}