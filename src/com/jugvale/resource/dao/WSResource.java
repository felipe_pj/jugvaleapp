package com.jugvale.resource.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import android.util.Log;

public abstract class WSResource {

	protected static final String LOG = "JugVALE";
	protected ObjectMapper objectMapper = null;
	protected JsonFactory jsonFactory = null;
	protected JsonParser jp = null;

	public WSResource() {
		objectMapper = new ObjectMapper();
		jsonFactory = new JsonFactory();
	}

	private String toString(InputStream is) throws IOException {

		byte[] bytes = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int lidos;
		while ((lidos = is.read(bytes)) > 0) {
			baos.write(bytes, 0, lidos);
		}
		return new String(baos.toByteArray());
	}

	protected String getRESTFileContent(String url) throws Exception {
		Log.i(LOG, "Acessando: " + url);
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url);

		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();

		if (entity != null) {
			InputStream instream = entity.getContent();
			String result = toString(instream);

			instream.close();
			return result;
		}
		return null;
	}
	
	protected String postRESTFileContent(String url, String json) throws Exception {
		Log.i(LOG, "Post: " + url + " JSON: " + json);
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		
		StringEntity input = new StringEntity(json);
		input.setContentType("application/json");
		httpPost.setEntity(input);
 
		HttpResponse response = httpClient.execute(httpPost);
		HttpEntity entity = response.getEntity();

		if (entity != null) {
			InputStream instream = entity.getContent();
			String result = toString(instream);

			instream.close();
			return result;
		}
		
		return null;
	}

}
