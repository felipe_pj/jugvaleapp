package com.jugvale.view.util;


import com.jugvale.view.R;


import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class AndroidUtils {
	protected static final String TAG = "INGOAL";
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public static void alertDialog(final Context context, final int mensagem) {
		try {
			AlertDialog dialog = new AlertDialog.Builder(context).setTitle(
					context.getString(R.string.app_name)).setMessage(mensagem).create();
			dialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			dialog.show();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
	}
	public static void alertDialog(final Context context, final String mensagem) {
		try {
			AlertDialog dialog = new AlertDialog.Builder(context).setTitle(
					context.getString(R.string.app_name)).setMessage(mensagem)
					.create();
			dialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			dialog.show();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
	}
	// Retorna se � Android 3.x "honeycomb" ou superior (API Level 11)
	public static boolean isAndroid_3() {
		int apiLevel = Build.VERSION.SDK_INT;
		if (apiLevel >= 11) {
			return true;
		}
		return false;
	}
	// Retorna se a tela � large ou xlarge
	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout
	            & Configuration.SCREENLAYOUT_SIZE_MASK)
	            >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	// Retona se � um tablet com Android 3.x
	public static boolean isAndroid_3_Tablet(Context context) {
	    return isAndroid_3() && isTablet(context);
	}
	// Fecha o teclado virtual se aberto (view com foque)
	public static boolean closeVirtualKeyboard(Context context, View view) {
		// Fecha o teclado virtual
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm != null) {
			boolean b = imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			return b;
		}
		return false;
	}
	// realiza uma notifica��o
	public static void notify(Context context, CharSequence messageStatusBar,
			CharSequence title, CharSequence message, Class<?> activity,
			int icon, Bundle bundle) {
		// recupera servi�o de notifica��o
		NotificationManager mn = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
		Notification n = new Notification(icon, messageStatusBar, System.currentTimeMillis());
		// pendingIntent para executar a Activity se o usu�iro clicar na notifica��o
		Intent intentMensagem  = new Intent(context, activity);
		intentMensagem.putExtras(bundle);
		PendingIntent p = PendingIntent.getActivity(context, 0, intentMensagem, 0);		
		// informa��es
		n.setLatestEventInfo(context, title, message, p);
		// vibra
		n.vibrate = new long[] {100,250,100,500};
		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		n.sound = alarmSound;		
		n.flags |= Notification.FLAG_AUTO_CANCEL;
		// notifica
		mn.notify(R.string.app_name, n);		
	}
	
	public static void cancelNotify(Context context, int message) {
		// recupera servi�o de notifica��o
		NotificationManager mn = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
		// cancela notifica��o
		mn.cancel(message);
	}
}
