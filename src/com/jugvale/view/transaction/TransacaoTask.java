package com.jugvale.view.transaction;


import com.jugvale.view.util.AndroidUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
/*
 * Classe usada para delegar tarefas de transacao para a interface Transacao
 * com os metodos executar() e atualizarView()
*/
public class TransacaoTask extends AsyncTask<Void, Void, Boolean>{
	
	private static final String TAG = "INGOAL";
	private final Context context;
	private final Transacao transacao;
	private ProgressDialog progresso;
	private Throwable exceptionErro;
	private int aguardeMsg;

	public TransacaoTask(Context context, Transacao transacao, int aguardeMsg) {
		super();
		this.context = context;
		this.transacao = transacao;
		this.aguardeMsg = aguardeMsg;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		abrirProgress();
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			transacao.executar();
		} catch (Throwable e) {
			// TODO: handle exception
			Log.e(TAG, e.getMessage(), e);
			// salva o erro e retorna false
			this.exceptionErro = e;
			return false;
		} finally {
			try {
				fecharProgress();
			} catch (Exception e) {
				// TODO: handle exception
				Log.e(TAG, e.getMessage(), e);
			}
		}
		
		return true;
	}
	

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		if(result) {
			// transacao executou sem falhas
			transacao.atualizarView();
		} else {
			// erro
			AndroidUtils.alertDialog(context, "Erro: " + exceptionErro.getMessage());
		}
	}

	private void abrirProgress() {
		// TODO Auto-generated method stub
		try {
			progresso = ProgressDialog.show(context, "", context.getString(aguardeMsg));
		} catch (Throwable e) {
			// TODO: handle exception
			Log.e(TAG, e.getMessage(), e);
		}
		
	}
	
	public void fecharProgress() {
		// TODO Auto-generated method stub
		try {
			if (progresso != null) 
				progresso.dismiss();
		} catch (Throwable e) {
			// TODO: handle exception
			Log.e(TAG, e.getMessage(), e);
		}
	}
	
}
