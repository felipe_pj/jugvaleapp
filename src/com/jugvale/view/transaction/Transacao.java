package com.jugvale.view.transaction;

public interface Transacao {

	// executar a transacao em uma thread separada
	public void executar() throws Exception;
	// atualizar a view sincronizada com a thread de interface
	public void atualizarView();
	
}
