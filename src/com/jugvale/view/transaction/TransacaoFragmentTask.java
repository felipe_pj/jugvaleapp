package com.jugvale.view.transaction;

import com.jugvale.view.fragment.JugValeFragment;
import com.jugvale.view.util.AndroidUtils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

public class TransacaoFragmentTask extends AsyncTask<Void, Void, Boolean> {

	private static final String TAG = "JugVale";
	private final Context context;
	private final Fragment fragement;
	private final Transacao transacao;
	private int progressId;
	private Exception exceptionErro;
	
	public TransacaoFragmentTask(JugValeFragment jugValeFragment,
			Transacao transacao, int progressId) {
		this.context = jugValeFragment.getActivity();
		this.fragement = jugValeFragment;
		this.transacao = transacao;
		this.progressId = progressId;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (result) {
			// transacao executou com sucesso
			transacao.atualizarView();
		} else {
			// Erro
			AndroidUtils.alertDialog(context, "Erro: " + exceptionErro.getMessage());
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		try {
			showProgress();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e(TAG, e.getMessage(), e);		
		} 		
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			transacao.executar();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			this.exceptionErro = e;
			return false;
		} finally {
			try {
				// sincroniza com a thread de interface
				fragement.getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						// ao final da transacao desliga o ProgressBar
						hideProgress();
					}
				});
				
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return true;
	}

	private void showProgress() {
		View view = fragement.getView();
		if (view != null) {
			ProgressBar progress = (ProgressBar) view.findViewById(progressId);
			if (progress != null) {
				progress.setVisibility(View.VISIBLE);
			}
		}
	}
	private void hideProgress() {
		View view = fragement.getView();
		if (view != null) {
			ProgressBar progress = (ProgressBar) view.findViewById(progressId);
			if (progress != null) {
				progress.setVisibility(View.INVISIBLE);
			}
		}
	}
	
	
}
