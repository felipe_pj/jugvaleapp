package com.jugvale.view.adapter;

import java.util.List;

import com.jugvale.model.PalestraPojo;
import com.jugvale.view.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AdapterPalestra extends BaseAdapter {
	protected static final String TAG = "INGOAL";
	private LayoutInflater inflater;
	private final List<PalestraPojo> palestras;
	private final Activity context;

	public AdapterPalestra(Activity context, List<PalestraPojo> palestras) {
		this.context = context;
		this.palestras = palestras;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return palestras != null ? palestras.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return palestras != null ? palestras.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolderEquipe holder = null;
		if (view == null) {
			holder = new ViewHolderEquipe();
			int layout = R.layout.list_item_palestra;
			view = inflater.inflate(layout, null);
			view.setTag(holder);
			holder.txtTitulo = (TextView) view.findViewById(R.id.txtTituloPalestra);
			holder.txtHora = (TextView) view.findViewById(R.id.txtHoraPalestra);
			holder.txtPalestrante = (TextView) view.findViewById(R.id.txtPalestrante);
		} else {
			holder = (ViewHolderEquipe) view.getTag();
		}
		PalestraPojo palestraPojo = palestras.get(position);

		holder.txtTitulo.setText(palestraPojo.getTitulo());
		holder.txtPalestrante.setText(palestraPojo.getNomePalestrante());
		holder.txtHora.setText(palestraPojo.getHoraApresentacao());
		return view;
	}

	static class ViewHolderEquipe {
		TextView txtTitulo;
		TextView txtHora;
		TextView txtPalestrante;
		ProgressBar progressPalestra;
	}
}
