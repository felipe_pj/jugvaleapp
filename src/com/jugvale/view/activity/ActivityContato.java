package com.jugvale.view.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.jugvale.view.R;
import com.jugvale.view.fragment.FragmentContato;

public class ActivityContato extends JugValeActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contato);
		if (savedInstanceState == null) {
			FragmentContato fragInfo = new FragmentContato();
			FragmentTransaction t = getSupportFragmentManager()
					.beginTransaction();
			t.add(R.id.layout_contato, fragInfo);
			t.commit();
		}
	}

}
