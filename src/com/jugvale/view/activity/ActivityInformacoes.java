package com.jugvale.view.activity;


import com.jugvale.view.R;
import com.jugvale.view.fragment.FragmentInformacoes;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

public class ActivityInformacoes extends JugValeActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_informacoes);
		if (savedInstanceState == null) {
			FragmentInformacoes fragInfo = new FragmentInformacoes();
			FragmentTransaction t = getSupportFragmentManager().beginTransaction();
			t.add(R.id.layout_informacoes, fragInfo);
			t.commit();
		}
	}

}
