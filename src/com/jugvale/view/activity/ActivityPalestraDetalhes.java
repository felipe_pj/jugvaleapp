package com.jugvale.view.activity;

import com.jugvale.view.R;
import com.jugvale.view.fragment.FragmentDetalhesPalestra;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

public class ActivityPalestraDetalhes extends JugValeActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalhes_palestra);
		if (savedInstanceState == null) {
			FragmentDetalhesPalestra fragDetalhesPelestra = new FragmentDetalhesPalestra();
			Bundle args = getIntent().getExtras();
			fragDetalhesPelestra.setArguments(args);
			FragmentTransaction t = getSupportFragmentManager().beginTransaction();
			t.add(R.id.layout_detalhes_palestra, fragDetalhesPelestra);
			t.commit();
		}
		
	}

}
