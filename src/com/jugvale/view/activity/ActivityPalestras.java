package com.jugvale.view.activity;


import com.jugvale.view.R;
import com.jugvale.view.fragment.FragmentListaPalestra;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

public class ActivityPalestras extends JugValeActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_palestras);
		if (savedInstanceState == null) {
			FragmentListaPalestra fragListaPalestra = new FragmentListaPalestra();
			FragmentTransaction t = getSupportFragmentManager().beginTransaction();
			t.add(R.id.layout_lista_palestra_esquerda, fragListaPalestra);
			t.commit();
		}
	}

}
