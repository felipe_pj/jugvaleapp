package com.jugvale.view.activity;


import com.jugvale.view.R;
import com.jugvale.view.fragment.FragmentInscricao;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

public class ActivityInscricao extends JugValeActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inscricao);
		if (savedInstanceState == null) {
			FragmentInscricao fragInscricao = new FragmentInscricao();
			FragmentTransaction t = getSupportFragmentManager().beginTransaction();
			t.add(R.id.layout_inscricao, fragInscricao);
			t.commit();
		}
	}

}
