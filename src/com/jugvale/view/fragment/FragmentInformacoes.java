package com.jugvale.view.fragment;

import com.jugvale.model.EventoPojo;
import com.jugvale.resource.IDAO;
import com.jugvale.resource.dao.EventoDAO;
import com.jugvale.view.R;
import com.jugvale.view.transaction.Transacao;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class FragmentInformacoes extends JugValeFragment implements
		Transacao {

	private IDAO<EventoPojo> eventoDAO;
	private EventoPojo evento;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_informacoes, null);
		View layouHeader = view.findViewById(R.id.layoutHeader);
		if (layouHeader != null) {
			layouHeader.setVisibility(View.GONE);
		}
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		eventoDAO = new EventoDAO();
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			EventoPojo eventoPojo = (EventoPojo) savedInstanceState.getSerializable(EventoPojo.KEY);
			Log.i(TAG, "Lendo estado: savedInstanceState(palestras)");
			this.evento = eventoPojo;
		}
		if (evento != null) {
			atualizarView();
		} else {
			startTransacao(this);
		}
	}
	
	@Override
	public void executar() throws Exception {
		this.evento = eventoDAO.list().get(0);
	}
	
	@Override
	public void atualizarView() {
		if (evento != null && getActivity() != null) {
			View view = getView();
			TextView txtEdicao = (TextView) view.findViewById(R.id.txtEdicao);
			txtEdicao.setText(evento.getEdicao());
			TextView txtDia = (TextView) view.findViewById(R.id.txtDia);
			txtDia.setText(evento.getDia());
			TextView txtHora = (TextView) view.findViewById(R.id.txtHora);
			txtHora.setText(evento.getHoraInicio() + " �s " + evento.getHoraFim());
			Log.i(TAG, "Exibindo evento: " + evento.getEdicao() + " Edi��o");
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.i(TAG, "Salvando Estado: onSaveInstanceState(bundle)");
		outState.putSerializable(EventoPojo.KEY, evento);
	}

}
