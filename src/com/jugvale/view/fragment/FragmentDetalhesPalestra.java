package com.jugvale.view.fragment;

import com.jugvale.model.PalestraPojo;
import com.jugvale.model.PalestrantePojo;
import com.jugvale.view.R;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentDetalhesPalestra extends JugValeFragment {

	private static final String TAG = "JUGVale";
	private PalestraPojo palestra;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_detalhes_palestra, null);
		View layouHeader = view.findViewById(R.id.layoutHeader);
		if (layouHeader != null) {
			layouHeader.setVisibility(View.GONE);
		}
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {
			palestra = (PalestraPojo) args.getSerializable(PalestraPojo.KEY);
			updateView();
		}
	}

	private void updateView() {
		View view = getView();
		
		TextView txtTitulo = (TextView) view.findViewById(R.id.txtTituloPalestra);
		txtTitulo.setText(palestra.getTitulo());
		
		TextView txtTema = (TextView) view.findViewById(R.id.txtTema);
		txtTema.setText(palestra.getTema());
		
		TextView txtHora = (TextView) view.findViewById(R.id.txtHora);
		txtHora.setText(palestra.getHoraApresentacao());
		
		TextView txtPalestrante = (TextView) view.findViewById(R.id.txtPalestrante);
		txtPalestrante.setText(palestra.getPalestrante().getNome());
		
		TextView txtEmail = (TextView) view.findViewById(R.id.txtEmail);
		txtEmail.setText(palestra.getPalestrante().getEmail());
		
		TextView txtMinicv = (TextView) view.findViewById(R.id.txtMinicv);
		txtMinicv.setText(palestra.getPalestrante().getMiniCV());
		
		Log.i(TAG, "Exibindo palestra: " + palestra.getTitulo());
	}

}
