package com.jugvale.view.fragment;

import com.jugvale.view.R;
import com.jugvale.view.transaction.Transacao;
import com.jugvale.view.transaction.TransacaoFragmentTask;
import com.jugvale.view.util.AndroidUtils;

import android.support.v4.app.Fragment;

public class JugValeFragment extends Fragment {

	protected static final String TAG = "JugVALE";
	private int progressId = R.id.progressPalestra;

	protected void alert(int mensagem) {
		AndroidUtils.alertDialog(getActivity(), mensagem);
	}

	// inicia a thread
	public void startTransacao(Transacao transacao) {
		boolean redeOk = AndroidUtils.isNetworkAvailable(getActivity());
		if (redeOk) {
			// inicia a transacao
			TransacaoFragmentTask task = new TransacaoFragmentTask(this,
					transacao, progressId);
			task.execute();
		} else {
			// nao existe conexao
			AndroidUtils.alertDialog(getActivity(),
					R.string.erro_conexao_indisponivel);
		}
	}

	public void setProgressId(int progressId) {
		this.progressId = progressId;
	}

}
