package com.jugvale.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.jugvale.view.R;

public class FragmentContato extends JugValeFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_contato, null);
		View layouHeader = view.findViewById(R.id.layoutHeader);
		if (layouHeader != null) {
			layouHeader.setVisibility(View.GONE);
		}
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		return view;
	}

}
