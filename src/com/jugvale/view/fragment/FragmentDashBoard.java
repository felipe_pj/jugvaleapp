package com.jugvale.view.fragment;

import com.jugvale.view.R;
import com.jugvale.view.activity.ActivityContato;
import com.jugvale.view.activity.ActivityInformacoes;
import com.jugvale.view.activity.ActivityInscricao;
import com.jugvale.view.activity.ActivityPalestras;
import com.jugvale.view.util.AndroidUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class FragmentDashBoard extends Fragment implements OnClickListener {

	// botoes do menu
	private Button btIncricao;
	private Button btPalestras;
	private Button btContato;
	private Button btInfo;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_dashboard, null);
		btIncricao = criarBotao(view, R.id.btInscricao);
		btPalestras = criarBotao (view, R.id.btPalestras);
		btContato = criarBotao(view, R.id.btContato);
		btInfo = criarBotao(view, R.id.btInfo);
		return view;

	}

	private Button criarBotao(View view, int id) {
		Button button = (Button) view.findViewById(id);
		button.setOnClickListener(this);
		return button;
	}

	@Override
	public void onClick(View v) {

		Context context = getActivity();
		if (AndroidUtils.isNetworkAvailable(context)) {

			String toast = "";
			if (v == btIncricao) {
				toast = "Inscri��o";
				incricao();
			}
			if (v == btPalestras) {
				toast = "Palestras";
				palestras();
			}
			if (v == btContato) {
				toast = "Contato";
				contato();
			}
			if (v == btInfo) {
				toast = "Info";
				info();
			}
			Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
		} else {
			AndroidUtils.alertDialog(context,
					R.string.erro_conexao_indisponivel);
		}

	}

	private void incricao() {
		Intent it = new Intent(getActivity(), ActivityInscricao.class);
		startActivity(it);
	}

	private void palestras() {
		Intent it = new Intent(getActivity(), ActivityPalestras.class);
		startActivity(it);
	}
	
	private void contato() {
		Intent it = new Intent(getActivity(), ActivityContato.class);
		startActivity(it);
	}
	
	private void info() {
		Intent it = new Intent(getActivity(), ActivityInformacoes.class);
		startActivity(it);
	}

}
