package com.jugvale.view.fragment;

import com.jugvale.model.MensagemRecurso;
import com.jugvale.model.UsuarioPojo;
import com.jugvale.resource.dao.InscricaoDAO;
import com.jugvale.view.R;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentInscricao extends JugValeFragment implements
		OnClickListener {
	private static final String LOG = "JUGVale";
	private InscricaoDAO inscricaoDAO;
	private TextView txtEmailUsuario;
	private TextView txtNomeUsuario;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(LOG, "onViewCreated() - inicio");
		View view = inflater.inflate(R.layout.fragment_inscricoes, null);
		View layouHeader = view.findViewById(R.id.layoutHeader);
		if (layouHeader != null) {
			layouHeader.setVisibility(View.GONE);
		}
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		Log.i(LOG, "onViewCreated() - instanciando dao");
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.i(LOG, "onViewCreated()");
		txtNomeUsuario = (TextView) view.findViewById(R.id.editNomeUsuario);
		txtEmailUsuario = (TextView) view.findViewById(R.id.editEmailUsuario);
		Button btInscricao = (Button) view.findViewById(R.id.btInscrever);
		btInscricao.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		inscricaoDAO = new InscricaoDAO();
		UsuarioPojo usuario = new UsuarioPojo();
		usuario.setNome(txtNomeUsuario.getText().toString());
		usuario.setEmail(txtEmailUsuario.getText().toString());
		Log.i(TAG, "(Incluindo Usu�rio: " + usuario.getNome() + " " + usuario.getEmail());
		try {
			MensagemRecurso mensagem = inscricaoDAO.cadastrar(usuario);
			String resultado = mensagem.getCodigo() + " " + mensagem.getMensagem();
			Toast.makeText(getActivity(), mensagem.getMensagem(), Toast.LENGTH_SHORT).show();
			Log.i(LOG, resultado);
		} catch (Exception e) {
			Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
			Log.e(LOG, "Erro ao cadastrar usu�rio");
		}
	}
}
