package com.jugvale.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.jugvale.model.EventoPojo;
import com.jugvale.model.ListaPalestra;
import com.jugvale.model.PalestraPojo;
import com.jugvale.resource.IDAO;
import com.jugvale.resource.dao.EventoDAO;
import com.jugvale.resource.dao.PalestraDAO;
import com.jugvale.resource.dao.HipoteticaDAO;
import com.jugvale.view.R;
import com.jugvale.view.activity.ActivityPalestraDetalhes;
import com.jugvale.view.adapter.AdapterPalestra;
import com.jugvale.view.transaction.Transacao;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class FragmentListaPalestra extends JugValeFragment implements
		OnItemClickListener, Transacao {

	protected ListView listView;
	protected List<PalestraPojo> palestras;
	private IDAO<EventoPojo> eventoDAO;

	@Override
	public void executar() throws Exception {
		this.palestras = new ArrayList<PalestraPojo>(eventoDAO.list().get(0).getPalestras());
	}

	@Override
	public void atualizarView() {
		if (palestras != null && !palestras.isEmpty() && getActivity() != null) {
			listView.setAdapter(new AdapterPalestra(getActivity(), palestras));
			PalestraPojo palestra = palestras.get(0);
			visualizarDetalhesPalestra(palestra, false);
		}
	}

	private void visualizarDetalhesPalestra(PalestraPojo palestra, boolean exibirDetalhes) {
		FragmentDetalhesPalestra fragDetalhesPalestra = new FragmentDetalhesPalestra();
		Bundle bundle = new Bundle();
		bundle.putSerializable(PalestraPojo.KEY, palestra);
		fragDetalhesPalestra.setArguments(bundle);
		View layoutDireita = getActivity().findViewById(
				R.id.layout_lista_palestra_direita);
		if (layoutDireita != null) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.replace(R.id.layout_lista_palestra_direita, fragDetalhesPalestra);
			ft.commit();
		} else {
			if (exibirDetalhes) {				
				Intent intent = new Intent(getActivity(), ActivityPalestraDetalhes.class);
				intent.putExtra(PalestraPojo.KEY, palestra);
				startActivity(intent);
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int posicao,
			long id) {
		PalestraPojo palestraPojo = (PalestraPojo) parent.getAdapter().getItem(posicao);
		visualizarDetalhesPalestra(palestraPojo, true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_lista_palestra, null);
		listView = (ListView) view.findViewById(R.id.listview_palestras);
		listView.setOnItemClickListener(this);
		setProgressId(R.id.progress_palestras);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		eventoDAO = new EventoDAO();
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			ListaPalestra lista = (ListaPalestra) savedInstanceState.getSerializable(ListaPalestra.KEY);
			Log.i(TAG, "Lendo estado: savedInstanceState(palestras)");
			this.palestras = lista.getPalestras();
		}
		if (palestras != null) {
			atualizarView();
		} else {
			startTransacao(this);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.i(TAG, "Salvando Estado: onSaveInstanceState(bundle)");
		outState.putSerializable(ListaPalestra.KEY, new ListaPalestra(palestras));
	}

}
